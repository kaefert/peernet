# PeerNet

For now this is just a development playground without any use for a user.

What this wants to become is a tool (platform?) for PeerToPeer communication.

Even this README.md file is not really constructed for conveying anything specific to a specific audience, it's more like an externalized thought process to help me organize and remember.

Nontheless, even if you are not me, you are welcome to read and comment on my thoughts if you happen to stumble upon this repository (for example by creating an issue here).

## Code found here for now

Playground.py + NetworkServer.py + NetworkTestClient.py are a UDP+TCP sample.
helloworld.pyx is a documented Cython (Python -> C -> binary) sample.


## Feature considerations

Where I live, most mobile networks use carrier grade NAT. Also battery concerns make it impractical to have a smartphone always listening for incoming connections.

Also, I'm one of those guys who actually like using devices of this bigger older design called laptop, especially for reading and writing longer messages and doing research, though smartphones are the best form factor for allowing quick notifications of incoming messages.

So we need a way to have multiple machines for a single identity and a mechanism for those machines to sync their message histories so that the user has a consistent view of their communication on each device.

That means there are (at least) two types of peers to handle: other machines of my user I need to keep in sync with, and actual contacts my user wants to exchange messages with. Probably also multiple devices per contact so that a message can be sent even if the device I originally added as contact is not online right now.

Public relays would also be nice (for users who only have battery-powered mobile devices and no stationary machine with a public ip). Those should be handled differently than secondary private machines of a contact in that it should not be able to read message contents. (end-to-end encryption)

Also non-contact peers (that my user is not expecting / accepting direct messages from) might be useful to employ for DHT [distributed hash tables] and non direct messaging features like search requests or requests for cached websites or message boards and stuff like that.


I did not yet find any existing PeerToPeer tool that has those features (especially the multiple machines per user thing). If you know of any, please tell me!

## Programming language choices

I'm still very uncertain on what languages, frameworks or APIs to use.
The first targets I want to support is Android and Linux servers, though I'd love to support even very weak Linux machines like OpenWRT routers.

I would very much prefer to not have to repeat myself (write the same logic in different languages for different platforms). Therefore some kind of cross platform approach would be nice.

On the surface C++ looks like it can be compiled for a lot of different platforms. The problem is, that it's a very ugly overly complex object-oriented extension to the otherwise nice C language.

Python exists for OpenWRT devices, though it's quite big storage wise which might make it unfeasible to use on cheap routers that don't have enough space.

### Web-based development for PeerToPeer software?!

Another idea would be to use HTML+CSS+JS+Cordova to target Android + Linux Servers powerful enough to run a Node.js Server. With the added benefit of easy porting to other platforms supported by Cordova (Like iOS & MacOS when I get my hands on a Mac Dev machine and find myself motivated to use it to that end) or maybe other website packaging tools.

OpenWRT or other low-powered Linux machines would then need a completely separate implementation.

Also I'm unsure of how potent Javascript+Cordava can be for PeerToPeer networking.
