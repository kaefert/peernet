#!/usr/bin/env python
# coding: utf8

''' Copyright (C) 2018 Thomas Käfer

	This file is part of PeerNet.

	PeerNet is free software: you can use, redistribute and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.

	PeerNet is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty
	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with PeerNet. If not, see <https://www.gnu.org/licenses/>.
'''

import socket

TARGET_IP = "127.0.0.1"
TARGET_PORT = 9331
MESSAGE = "Hello, World!"

def probeUdpServer(): 
	sock = socket.socket(socket.AF_INET, # Internet
						socket.SOCK_DGRAM) # UDP
	sock.sendto(MESSAGE.encode('utf-8'), (TARGET_IP, TARGET_PORT))
	data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
	print("received reply from ", addr, "; message:", data)

def testTcpServer():
	sock = socket.socket(socket.AF_INET,
						socket.SOCK_STREAM) # TCP
	sock.connect((TARGET_IP, TARGET_PORT))
	sock.send(MESSAGE.encode('utf-8'))
	print("received reply from ", sock.getpeername(), "; message:", sock.recv(1024))
	sock.send("that's very nice of you, thanks!".encode('utf-8'))
	print("received reply from ", sock.getpeername(), "; message:", sock.recv(1024))
	#print(val)

	sock.close()
