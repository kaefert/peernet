#!/usr/bin/env python
# coding: utf8

''' Copyright (C) 2018 Thomas Käfer

	This file is part of PeerNet.

	PeerNet is free software: you can use, redistribute and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.

	PeerNet is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty
	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with PeerNet. If not, see <https://www.gnu.org/licenses/>.
'''


# http://masnun.rocks/2016/10/01/creating-an-executable-file-using-cython/
#cython --embed -o helloworld.c helloworld.pyx
#gcc -o helloworld helloworld.c
#gcc -v -Os -I /Users/masnun/.pyenv/versions/3.5.1/include/python3.5m -L /usr/local/Frameworks/Python.framework/Versions/3.5/lib  -o test test.c  -lpython3.5  -lpthread -lm -lutil -ldl

# https://stackoverflow.com/questions/22507592/making-an-executable-in-cython
#gcc -Os -I /usr/include/python2.7 helloworld.c -lpython2.7 -o helloworld

print("Hello World")