#!/usr/bin/env python
# coding: utf8

''' Copyright (C) 2018 Thomas Käfer

	This file is part of PeerNet.

	PeerNet is free software: you can use, redistribute and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.

	PeerNet is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty
	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with PeerNet. If not, see <https://www.gnu.org/licenses/>.
'''

import socket
import select
import threading
import time

BUFFER_SIZE = 1024

def defaultReceiver(source, addr, data=None):
	if data == None:
		print("new connection opened from ", addr)
		source.sendto("thanks for opening this connection. welcome :)".encode('utf-8'), addr)
	else:
		print("received message from ", addr, "; message: ", data)
		source.sendto("confirming that I got this from you: ".encode('utf-8') + data, addr)



class NetworkServer:

	def listenLoop(self, receiver):
		while (not self.stopped):
			#print("NetworkServer not yet stopped. continue listening for next connection!")
			idle=True
			with self.lock:
				ready = select.select(self.allSocks, [], [], 1)
				for r in ready:
					if r == []:
						continue #ignore those
					elif r[0] == self.udp:
						# UDP ready for read
						data, addr = self.udp.recvfrom(BUFFER_SIZE)
						receiver(self.udp, addr, data)
						idle=False
					elif r[0] == self.tcp:
						# new tcp connection request
						sock, addr = self.tcp.accept()
						self.allSocks.append(sock)
						receiver(sock, addr)
						idle=False
					else:
						for sock in self.allSocks:
							if sock.fileno() == -1:
								#socket closed, forget it.
								self.allSocks.remove(sock)
							elif r[0] == sock:
								# existing tcp connection ready for read
								try:
									receiver(r[0], r[0].getpeername(), r[0].recv(BUFFER_SIZE))
								except OSError as e:
									if e.errno in [104, 107]:
										# [Errno 104] Connection reset by peer
										# [Errno 107] Transport endpoint is not connected
										# have been observed when remote closed connection. close & remove socket from my list.
										sock.close()
										self.allSocks.remove(sock)
									else:
										print(e)
								idle=False
								break

						if idle:
							print("Error! unexpected element in ready array: ", r)

			#if ready[0]: #recieved udp packet
			#if ready[1]: #received new tcp connection request
			
			#for k in range(0,len(self.openCons)):
			#    if ready[k+2]:
			#        data = self.openCons[k].recv(BUFFER_SIZE)
			#        receiver(self.openCons[k], self.conAddrs[k], data)

			if idle:
				time.sleep(0.5)
		print("NetworkServer loop stopped.")        

	def __init__(self, ip: str = "0.0.0.0", port: int = 9331, receiver = defaultReceiver):
		self.stopped = False
		self.ip = ip
		self.port = port
		self.udp = socket.socket(socket.AF_INET, # Internet
								socket.SOCK_DGRAM) # UDP
		self.udp.bind((ip, port))
		self.udp.setblocking(0)
		
		self.tcp = socket.socket(socket.AF_INET, 
								socket.SOCK_STREAM) #TCP
		self.tcp.bind((ip, port))
		self.tcp.listen()
		
		self.allSocks = [self.udp, self.tcp]
		
		if(receiver is defaultReceiver):
			print("Warning: no custom receiver given. Will fallback to printing received data and answering 'gotcha' to everybody")
		
		self.lock = threading.Lock()
		self.thread = threading.Thread(target=self.listenLoop, args=(receiver,))
		self.thread.start()

	def stop(self):
		with self.lock:
			self.stopped = True
			for sock in self.allSocks:
				sock.close()
				self.allSocks.remove(sock)
