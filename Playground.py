#!/usr/bin/env python
# coding: utf8

''' Copyright (C) 2018 Thomas Käfer

	This file is part of PeerNet.

	PeerNet is free software: you can use, redistribute and/or modify
	it under the terms of the GNU Affero General Public License as
	published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.

	PeerNet is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty
	of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	See the GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with PeerNet. If not, see <https://www.gnu.org/licenses/>.
'''

import NetworkServer
import time
import NetworkTestClient

def CLI():
	print("Hello World!");
	server = NetworkServer.NetworkServer()
	print("Server started");
	time.sleep(1)
	print("Slept a second, probing server now")
	NetworkTestClient.probeUdpServer()
	NetworkTestClient.testTcpServer()
	print("probing done")
	time.sleep(5)
	print("stoping server")
	server.stop()
	print("goodbye!")


if __name__ == '__main__':
	CLI();
